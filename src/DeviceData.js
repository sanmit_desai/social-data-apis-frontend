import React from "react";
import style from "./DevicData.module.css";
import "./App.css";

const DeviceData = ({ title, description, thumbNail, link, ratingValue }) => {
  return (
    <div>
      <div className="Title">
        <a href={link}>{title}</a>
      </div>
      <div className="Thumbnaildiv">
        <img className="Thumbail" src={thumbNail} height="40" width="60" />
      </div>
      <div className="Description">
        <p>{description}</p>
      </div>{" "}
    </div>
  );
};

export default DeviceData;
