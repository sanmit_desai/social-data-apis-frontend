// import React, { useEffect, useState } from "react";
// import "./App.css";

// const ExampleReq = () => {
//   // useEffect(() => {
//   //   getData();
//   // }, []);

//   const [data, setData] = useState(null);
//   const [inputValue, setInputvalue] = useState("");
//   const getData = async () => {
//     if (inputValue.includes("oneplus 8")) {
//       const response = await fetch(
//         `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=oneplus 8`
//       );
//       const temp = await response.json();
//       if (temp) {
//         setData(temp);
//       }
//     } else {
//       setData(null);
//     }
//   };

//   return (
//     <div className="App">
//       {data && <h1>{data.output.video_reviews[0].title}</h1>}
//       <input
//         type="text"
//         value={inputValue}
//         onChange={(e) => {
//           setInputvalue(e.target.value);
//         }}
//       />
//       <button type="submit" form="form1" value="Submit" onClick={getData}>
//         Submit
//       </button>
//     </div>
//   );
// };

// export default ExampleReq;

// import React, { useEffect, useState } from "react";
// import "./App.css";

// const ExampleReq = () => {
//   // useEffect(() => {
//   //   getData();
//   // }, []);

//   const [data, setData] = useState(null);
//   const [inputValue, setInputvalue] = useState("");
//   const getData = async () => {
//     if (inputValue.includes("oneplus 8")) {
//       const response = await fetch(
//         `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=oneplus 8`
//       );
//       const temp = await response.json();
//       if (temp) {
//         setData(temp);
//       }
//     } else {
//       setData(null);
//     }
//   };

//   return (
//     <div className="App">
//       {data && <h1>{data.output.video_reviews[0].title}</h1>}
//       <input
//         type="text"
//         value={inputValue}
//         onChange={(e) => {
//           setInputvalue(e.target.value);
//         }}
//       />
//       <button type="submit" form="form1" value="Submit" onClick={getData}>
//         Submit
//       </button>
//     </div>
//   );
// };

// export default ExampleReq;

import React, { useState, useEffect } from "react";
import "./App.css";
import DeviceData from "./DeviceData";
import StarRatings from "react-star-ratings";
import RatingData from "./ratingData";
import HSBar from "react-horizontal-stacked-bar-chart";

const App = () => {
  const [data, setData] = useState(null);
  const [device, setDevice] = useState("");
  // const [rating, setRating] = useState();
  const [search, setSearch] = useState("");

  // const [query, setQuery] = useState(" ");

  // const [datas1, setDatas1] = useState([]);
  // const [search1, setSearch1] = useState("");
  // const [query1, setQuery1] = useState(" ");

  // useEffect(() => {
  //   getReview();
  // }, [query]);

  // useEffect(() => {
  //   getData1();
  // }, [query1]);

  // const getMode = () => {
  //   if (search.includes("one plus 8") || search.includes("oneplus 8")) {
  //     setDevice("oneplus 8");
  //     return 1;
  //   } else if (search.includes("iphone 11 pro max")) {
  //     setDevice("iphone 11 pro max");
  //     return 1;
  //   } else if (
  //     search.includes("oneplus 8" && "unboxing") ||
  //     search.includes("unboxing")
  //   ) {
  //     return 2;
  //   }
  //   else if(
  //     search.includes("iphone 11 pro max"+"unboxing")
  //   ){return 2} else if (search.includes("user ratings")) {
  //     return 3;
  //   } else {
  //     return 0;
  //   }
  // };

  const getMode = () => {
    if (search.includes("one plus 8") || search.includes("oneplus 8")) {
      setDevice("oneplus 8");
    } else if (search.includes("iphone 11 pro max")) {
      setDevice("iphone 11 pro max");
    } else if (search.includes("iphone 11 pro")) {
      setDevice("iphone 11 pro");
    } else if (search.includes("iphone 11")) {
      setDevice("iphone 11");
    } else if (search.includes("samsung s20 ultra")) {
      setDevice("samsung s20 ultra");
    }

    if (search.includes("user ratings")) {
      return 3;
    } else if (search.includes("unboxing")) {
      return 2;
    } else if ((search.includes("review")) || (search.includes("reviews"))) {
      return 1;
    } else {
      return 0;
    }
  };

  const getSearch = async () => {
    const mode = getMode();
    let response = null;
    switch (mode) {
      case 1: {
        response = await fetch(
          `http://13.58.20.207:7015/api/review/retrieve/v1?device_name=${device}&type=summary`
        );

        response = await response.json();
        break;
      }
      case 2: {
        response = await fetch(
          `http://13.58.20.207:7015/api/video/retrieve/v1?device_name=${device}&context=unboxing`
        );
        response = await response.json();
        break;
      }

      case 3: {
        response = await fetch(
          `http://13.58.20.207:7015/api/review/retrieve/v1?device_name=${device}&type=user_rating&review_count=5`
        );
        response = await response.json();
        break;
      }
      // case 4: {
      //   response = await fetch(
      //     `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=iphone 11 pro max`
      //   );
      //   response = await response.json();
      //   break;
      // }
    }

    setData(response);

    console.log("response", response);
  };

  // const getData1 = async () => {
  //   const response1 = await fetch(
  //     `http://13.58.20.207:7015/api/video/retrieve/v1?device_name=oneplus 8&context=${query1}`
  //   );
  //   const tempData1 = await response1.json();
  //   setDatas1(tempData1.output.scrape);
  //   console.log("scrape unboxing", tempData1.output.scrape);
  // };

  const updateSearch = (e) => {
    setSearch(e.target.value);
    console.log(search);
  };

  // const getSearch = (e) => {
  //   e.preventDefault();
  //   if (search.includes("one plus 8") || search.includes("oneplus 8")) {
  //     setQuery("oneplus 8");
  //   } else if (
  //     search.includes("one plus 8 unboxing") ||
  //     search.includes("unboxing")
  //   ) {
  //     setQuery1("unboxing");
  //     console.log("queryyyyy", query);
  //     console.log("queryyy1", query1);
  //   }
  //   setSearch("");
  // };

  return (
    <div className="App">
      <input
        className="search-bar"
        type="text"
        onKeyDown={(e) => {
          if (e.keyCode == 13) {
            getSearch();
          }
        }}
        value={search}
        onChange={updateSearch}
      />
      <button className="search-button" onClick={getSearch}>
        Submit
      </button>

      {!data && (
        <div>
          <h1>No Data Available</h1>
        </div>
      )}

      {data && (
        <div className="reviews">
          {data.output && data.output.video_reviews && (
            <div>
              <h4>{device} has </h4>

              <StarRatings
                starRatedColor="#FFD700"
                numberOfStars={5}
                rating={data.output.product_rating}
                name="rating"
                starDimension="35"
              />
              <h4>{data.output.product_rating} User Rating</h4>
              <h4>Here are some videos reviewing {device}</h4>

              {data.output.video_reviews.map((item) => (
                <DeviceData
                  key={item.title}
                  title={item.title}
                  thumbNail={item.thumbnail.static}
                  description={item.description}
                  link={item.link}
                />
              ))}
            </div>
          )}

          {data.output && data.output.scrape && (
            <div>
              <div>
                <h4>Here are some videos on {device} unboxing </h4>

                {data.output.scrape.map((item) => (
                  <DeviceData
                    key={item.title}
                    title={item.title}
                    thumbNail={item.thumbnail.static}
                    description={item.description}
                    link={item.link}
                  />
                ))}
              </div>
              )
            </div>
          )}
          {data.output && data.output.user_reviews && (
            <div>
              <h4>This is what users think about {device} </h4>
              <div
                style={{
                  float: "left",
                  alignContent: "center",
                  justifyContent: "center",
                }}
              >
                <h1
                  style={{
                    fontSize: "40px",
                    textAlign: "center",
                    justifyContent: "center",
                  }}
                >
                  {data.output.average_rating_5}
                </h1>
                <StarRatings
                  starRatedColor="#FFD700"
                  numberOfStars={5}
                  rating={data.output.average_rating_5}
                  name="rating"
                  starDimension="35"
                />
              </div>

              <div
                style={{
                  width: "40%",
                  border: "1px solid black",
                }}
              >
                <HSBar
                  height={8}
                  data={[
                    { value: 10000, color: "#FFD700" },
                    { value: 2000, color: "#F8F8F8" },
                  ]}
                />
                <HSBar
                  height={8}
                  data={[
                    { value: 1000, outlineWidth: 30, color: "#FFD700" },
                    { value: 2000, color: "#F8F8F8" },
                  ]}
                />
                <HSBar
                  height={8}
                  data={[
                    { value: 1000, outlineWidth: 30, color: "#FFD700" },
                    { value: 2000, color: "#F8F8F8" },
                  ]}
                />
                <HSBar
                  height={8}
                  data={[
                    { value: 1000, outlineWidth: 30, color: "#FFD700" },
                    { value: 2000, color: "#F8F8F8" },
                  ]}
                />
                <HSBar
                  height={8}
                  data={[
                    { value: 1000, outlineWidth: 30, color: "#FFD700" },
                    { value: 2000, color: "#F8F8F8" },
                  ]}
                />
              </div>

              {data.output.user_reviews.map((item) => (
                <RatingData
                  key={item.title}
                  title={item.title}
                  description={item.content}
                  link={item.source}
                  ratingValue={item.rating}
                  dateCreated={item.date}
                />
              ))}
            </div>
          )}
        </div>
      )}
    </div>
  );

  //   if (query("oneplus 8")) {
  //     return (
  //       <div className="App">
  //         <form onSubmit={getSearch} className="search-form">
  //           <input
  //             className="search-bar"
  //             type="text"
  //             value={search}
  //             onChange={updateSearch}
  //           />
  //           <button className="search-button" type="submit">
  //             Submit
  //           </button>
  //         </form>
  //         {!datas ? (
  //           ""
  //         ) : (
  //           <from>
  //             <h4>One Plus 8 has </h4>
  //             <h4>{rating} User Rating</h4>
  //             <StarRatings
  //               starRatedColor="#FFD700"
  //               numberOfStars={5}
  //               rating={!datas ? "" : rating}
  //               name="rating"
  //             />
  //           </from>
  //         )}
  //         {}
  //         {!datas ? (
  //           <h1 className="sorry">Sorry, No Results found</h1>
  //         ) : (
  //           datas.map((data) => (
  //             <DeviceData
  //               key={data.title}
  //               title={data.title}
  //               thumbNail={data.thumbnail.static}
  //               description={data.description}
  //               link={data.link}
  //             />
  //           ))
  //         )}
  //       </div>
  //     );
  //   } else if (query("unboxing")) {
  //     return (
  //       <div className="App">
  //         <form onSubmit={getSearch} className="search-form">
  //           <input
  //             className="search-bar"
  //             type="text"
  //             value={search1}
  //             onChange={updateSearch}
  //           />
  //           <button className="search-button" type="submit">
  //             Submit
  //           </button>
  //         </form>
  //         {!datas1 ? (
  //           ""
  //         ) : (
  //           <from>
  //             <h4> Here are some video for One Plus 8 Unboxing </h4>
  //           </from>
  //         )}
  //         {}
  //         {!datas1 ? (
  //           <h1 className="sorry">Sorry, No Results found</h1>
  //         ) : (
  //           datas1.map((data1) => (
  //             <DeviceData
  //               key={data1.title}
  //               title={data1.title}
  //               thumbNail={data1.thumbnail.static}
  //               description={data1.description}
  //               link={data1.link}
  //             />
  //           ))
  //         )}
  //       </div>
  //     );
  //   }
};

export default App;
