// import React, { useEffect, useState } from "react";
// import "./App.css";

// const ExampleReq = () => {
//   // useEffect(() => {
//   //   getData();
//   // }, []);

//   const [data, setData] = useState(null);
//   const [inputValue, setInputvalue] = useState("");
//   const getData = async () => {
//     if (inputValue.includes("oneplus 8")) {
//       const response = await fetch(
//         `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=oneplus 8`
//       );
//       const temp = await response.json();
//       if (temp) {
//         setData(temp);
//       }
//     } else {
//       setData(null);
//     }
//   };

//   return (
//     <div className="App">
//       {data && <h1>{data.output.video_reviews[0].title}</h1>}
//       <input
//         type="text"
//         value={inputValue}
//         onChange={(e) => {
//           setInputvalue(e.target.value);
//         }}
//       />
//       <button type="submit" form="form1" value="Submit" onClick={getData}>
//         Submit
//       </button>
//     </div>
//   );
// };

// export default ExampleReq;

// import React, { useEffect, useState } from "react";
// import "./App.css";

// const ExampleReq = () => {
//   // useEffect(() => {
//   //   getData();
//   // }, []);

//   const [data, setData] = useState(null);
//   const [inputValue, setInputvalue] = useState("");
//   const getData = async () => {
//     if (inputValue.includes("oneplus 8")) {
//       const response = await fetch(
//         `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=oneplus 8`
//       );
//       const temp = await response.json();
//       if (temp) {
//         setData(temp);
//       }
//     } else {
//       setData(null);
//     }
//   };

//   return (
//     <div className="App">
//       {data && <h1>{data.output.video_reviews[0].title}</h1>}
//       <input
//         type="text"
//         value={inputValue}
//         onChange={(e) => {
//           setInputvalue(e.target.value);
//         }}
//       />
//       <button type="submit" form="form1" value="Submit" onClick={getData}>
//         Submit
//       </button>
//     </div>
//   );
// };

// export default ExampleReq;

import React, { useEffect, useState } from "react";
import "./App.css";
import DeviceData from "./DeviceData";
import StarRatings from "react-star-ratings";

const App = () => {
  const [datas, setDatas] = useState([]);
  const [rating, setRating] = useState();
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState(" ");

  useEffect(() => {
    getReview();
  }, [query]);

  const getReview = async () => {
    const response = await fetch(
      `http://13.58.20.207:7015/api/review/retrieve/all/v1?device_name=${query}`
    );
    const tempData = await response.json();
    setDatas(tempData.output.video_reviews);
    setRating(tempData.output.product_rating);
    console.log("videos_Reviews", tempData.output.video_reviews);
  };
  const updateSearch = (e) => {
    setSearch(e.target.value);
    console.log(search);
  };

  const getSearch = (e) => {
    e.preventDefault();
    if (search.includes("one plus 8") || search.includes("oneplus 8")) {
      setQuery("oneplus 8");
    } else {
      setQuery(search);
    }
    setSearch("");
  };

  return (
    <div className="App">
      <form onSubmit={getSearch} className="search-form">
        <input
          className="search-bar"
          type="text"
          value={search}
          onChange={updateSearch}
        />
        <button className="search-button" type="submit">
          Submit
        </button>
      </form>
      {!datas ? (
        ""
      ) : (
        <from>
          <h4>One Plus 8 has </h4>
          <h4>{rating} User Rating</h4>
          <StarRatings
            starRatedColor="#FFD700"
            numberOfStars={5}
            rating={!datas ? "" : rating}
            name="rating"
          />
        </from>
      )}
      {}
      {!datas ? (
        <h1 className="sorry">Sorry, No Results found</h1>
      ) : (
        datas.map((data) => (
          <DeviceData
            key={data.title}
            title={data.title}
            thumbNail={data.thumbnail.static}
            description={data.description}
            link={data.link}
          />
        ))
      )}
    </div>
  );
};

export default App;
