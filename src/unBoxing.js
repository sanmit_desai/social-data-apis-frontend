import React, { useEffect, useState } from "react";
import "./App.css";
import DeviceData from "./DeviceData";

const Unboxing = () => {
  const [datas1, setDatas1] = useState([]);
  const [search1, setSearch1] = useState("");
  const [query1, setQuery1] = useState(" ");

  useEffect(() => {
    getData1();
  }, [query1]);

  const getData1 = async () => {
    const response1 = await fetch(
      `http://13.58.20.207:7015/api/video/retrieve/v1?device_name=oneplus 8&context=${query}`
    );
    const tempData1 = await response.json();
    setDatas(tempData1.output.scrape);
    console.log("scrape unboxing", tempData1.output.scrape);
  };
  const updateSearch1 = (e) => {
    setSearch1(e.target.value);
    console.log(search1);
  };

  const getSearch1 = (e) => {
    e.preventDefault();
    if (search1.includes("unboxing")) {
      setQuery1("unboxing");
    } else {
      setQuery1(search);
    }
    setSearch1("");
  };

  return (
    <div className="App">
      <form onSubmit={getSearch1} className="search-form">
        <input
          className="search-bar"
          type="text"
          value={search1}
          onChange={updateSearch1}
        />
        <button className="search-button" type="submit">
          Submit
        </button>
      </form>
      {!datas1 ? (
        ""
      ) : (
        <from>
          <h4> Here are some video for One Plus 8 Unboxing </h4>
          <h4>{rating} User Rating</h4>
        </from>
      )}
      {}
      {!datas1 ? (
        <h1 className="sorry">Sorry, No Results found</h1>
      ) : (
        datas1.map((data1) => (
          <DeviceData
            key={data.title}
            title={data.title}
            thumbNail={data.thumbnail.static}
            description={data.description}
            link={data.link}
          />
        ))
      )}
    </div>
  );
};

export default Unboxing;
