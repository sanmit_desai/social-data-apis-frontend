import React from "react";
import StarRatings from "react-star-ratings";

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 5,
    }}
  />
);
const RatingData = ({ title, description, link, ratingValue, dateCreated }) => {
  return (
    <div>
      <div
        style={{
          justifyContent: "center",
          textAlign: "start",
          width: "700px",
          borderBlockColor: "black",
        }}
      >
        <ColoredLine color="grey" />
        <h4>{dateCreated}</h4>
        <h4>{title}</h4>
        <StarRatings
          starRatedColor="#FFD700"
          numberOfStars={5}
          rating={ratingValue}
          name="rating"
        />

        <p>{description}</p>
        <ColoredLine color="grey" />
      </div>
    </div>
  );
};

export default RatingData;
